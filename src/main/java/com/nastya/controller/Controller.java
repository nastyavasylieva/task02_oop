package com.nastya.controller;

import com.nastya.model.Bouquet;
import com.nastya.model.Flower;
import com.nastya.model.FlowerSeller;

import java.util.List;
import java.util.Scanner;

public class Controller {
    private Scanner scanner;
    private FlowerSeller flowerSeller;
    private Bouquet bouquet;
    private Flower flower;

    public Controller() {
        flowerSeller = new FlowerSeller();
        bouquet = new Bouquet();
    }

    public List<Flower> getShopList() {
        return flowerSeller.getShopList();
    }

    public void addToOrder(Flower flower, int count) {
        bouquet.addFlower(flower, count);
    }

    public void printOrderedBouquet() {
        bouquet.printBouquet();
    }

}
