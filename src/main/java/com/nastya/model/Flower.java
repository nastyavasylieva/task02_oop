package com.nastya.model;

import java.util.Objects;

public class Flower {
    private String name;
    private int price;
    private String color;
    private boolean inFlowerpot;

    public Flower() {
    }

    public Flower(String name, int price, String color, boolean inFlowerpot) {
        this.name = name;
        this.price = price;
        this.color = color;
        this.inFlowerpot = inFlowerpot;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public boolean isInFlowerpot() {
        return inFlowerpot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flower)) return false;
        Flower flower = (Flower) o;
        return getPrice() == flower.getPrice() &&
                isInFlowerpot() == flower.isInFlowerpot() &&
                Objects.equals(getName(), flower.getName()) &&
                Objects.equals(getColor(), flower.getColor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPrice(), getColor(), isInFlowerpot());
    }

    @Override
    public String toString() {
        return name + ": color - " + color + ", price - " + price + (inFlowerpot ? ", in a flowerpot" : ".");
    }
}
