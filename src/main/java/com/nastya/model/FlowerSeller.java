package com.nastya.model;

import java.util.ArrayList;
import java.util.List;

public class FlowerSeller {
    List<Flower> shopList = new ArrayList<Flower>();

    public FlowerSeller() {
        fillShopList();
    }

    public void fillShopList() {
        shopList.add(0, new Flower("Rose", 40, "red", false));
        shopList.add(1, new Flower("Tulip", 20, "violet", false));
        shopList.add(2, new Flower("Tulip", 25, "yellow", false));
        shopList.add(3, new Flower("Orchid", 100, "white", true));
        shopList.add(4, new Flower("Iris", 30, "pink", false));
        shopList.add(5, new Flower("Rose", 40, "white", false));
        shopList.add(6, new Flower("Peony", 35, "pink", false));
        shopList.add(7, new Flower("Daisy", 60, "white", true));
    }

    public List<Flower> getShopList() {
        return shopList;
    }

}
