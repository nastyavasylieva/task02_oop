package com.nastya.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Bouquet {
    Comparator<Flower> comparator = new Comparator<Flower>() {
        public int compare(Flower o1, Flower o2) {
            if (o1.getPrice() == o2.getPrice()) {
                return 0;
            } else if (o1.getPrice() > o1.getPrice()) {
                return 1;
            } else {
                return -1;
            }
        }
    };
    private int price;
    private List<Flower> flowerList = new ArrayList<Flower>();

    public Bouquet() {
    }

    public Bouquet(List<Flower> flowerList) {
        this.flowerList = flowerList;
        price = 0;
        for (Flower f :
                flowerList) {
            price += f.getPrice();
        }
    }

    public List<Flower> getFlowerList() {
        return flowerList;
    }

    public int getPrice() {
        return price;
    }

    public void addFlower(Flower flower, int count) {
        for (int i = 0; i < count; i++) {
            flowerList.add(flower);
            price += flower.getPrice();

        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bouquet)) return false;
        Bouquet bouquet = (Bouquet) o;
        return getPrice() == bouquet.getPrice() &&
                Objects.equals(getFlowerList(), bouquet.getFlowerList()) &&
                Objects.equals(comparator, bouquet.comparator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFlowerList(), getPrice(), comparator);
    }

    public void printBouquet() {
        flowerList.sort(comparator);
        for (Flower f :
                flowerList) {
            System.out.println(f);
        }
        System.out.println("Price = " + price);
    }
}
