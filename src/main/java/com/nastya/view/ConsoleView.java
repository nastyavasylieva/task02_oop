package com.nastya.view;

import com.nastya.controller.Controller;
import com.nastya.model.Flower;

import java.util.List;
import java.util.Scanner;

public class ConsoleView {
    Scanner scanner;
    Controller controller;

    public ConsoleView() {
        controller = new Controller();
    }

    public void start() {
        scanner = new Scanner(System.in);
        showMenu();
    }

    public void showMenu() {
        System.out.println("Welcome to our Flower shop!");
        System.out.println("1. See flowers list.");
        System.out.println("2. Buy a bouquet.");
        System.out.println("3. Buy a flower in a flowerpot");
        System.out.println("4. Exit.");

        while (true) {
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    showShopList();
                    break;
                case 2:
                    buyBouquet();
                    break;
                case 3:
                    buyFlowerpot();
                    break;
                case 4:
                    return;
            }
        }
    }

    public void showShopList() {
        List<Flower> list = controller.getShopList();
        int i = 0;
        for (Flower f :
                list) {
            System.out.println(i + 1 + ". " + f);
            i++;
        }
    }

    public void buyBouquet() {
        System.out.println("To add flower to bouquet write number and count of the flower");
        boolean continueScanning = true;
        while (continueScanning) {
            int flowerId = scanner.nextInt() - 1;
            int flowerCount = scanner.nextInt();
            controller.addToOrder(controller.getShopList().get(flowerId), flowerCount);
            System.out.println("One more flower? (y/n)");
            String answer = scanner.nextLine();
            if (!answer.equalsIgnoreCase("y")) {
                continueScanning = false;
                controller.printOrderedBouquet();
            }
        }
    }

    public void buyFlowerpot() {
        System.out.println("To buy flowerpot enter id of the flower");
        int flowerId = scanner.nextInt() - 1;
        Flower orderedFlower = controller.getShopList().get(flowerId);
        if (!orderedFlower.isInFlowerpot()) {
            System.out.println("This flower is not in a flowerpot!");
        } else {
            System.out.println("Your order:" + orderedFlower);
            return;
        }
    }
}
