package com.nastya;

import com.nastya.view.ConsoleView;

public class Dispatcher {
    public static void main(String[] args) {
        ConsoleView view = new ConsoleView();
        view.start();
    }
}
